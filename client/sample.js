import React from "react";
import { useEffect, useState } from "react";
import { Formik } from "formik";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import * as yup from "yup";
import io from "socket.io-client";
import "./ChatRoomPage.css";
import { getChatRoomMessages, getChatRooms } from "./requests";
const SOCKET_IO_URL = "https://naada-socket.herokuapp.com";
const socket = io.connect(SOCKET_IO_URL, {transports:['websocket']});

const getChatData = () => {
  return {
    chatRoomName: 'socket_1',  // this is the unique streamId
    userName: 'kahsyap',
    userAvatar: 'avatar'
  }
  return JSON.parse(localStorage.getItem("chatData"));
};

const schema = yup.object({
  message: yup.string().required("Message is required"),
});

function ChatRoomPage() {
  const [initialized, setInitialized] = useState(false);
  const [messages, setMessages] = useState([]);
  const [rooms, setRooms] = useState([]);


  const handleSubmit = async evt => {
    const data = Object.assign({}, evt);
    data.chatRoomName = 'socket_1';
    data.user = {
      "userId": 2136,
      "userName": "kashyap_yets",
      "userAvatar": "test.com"
  };
    data.message = evt.message;
    console.log('message sent', data)
    socket.emit("message", data);
  };


  // intialize the socket connection on page loading.
  // you'll get the chatroom name from different api from api-server
  // replace the user and chatromm data from  the stream data.
  const connectToRoom = () => {
    console.log('connect_to_romm', Object.keys(socket))
    socket.on("connect", () => {
      const data = {
        chatRoomName: getChatData().chatRoomName,
        user:{
          userId: 123456,
          userName: getChatData().userName,
          userAvatar: getChatData().userAvatar
        }
      }
      socket.emit("join", data);
    });
    
    // acknowledgement as room joined so you can show message in chat like:
    // ex: 'Mr. hello joined the event'
    socket.on("roomJoined", (data) => {
      console.log('romm joined successfully', data)
    })

    // when new user join the room you need to update the chat list.
    // you'll receive the entire updatedchatlist
    socket.on("updateChatList", (data) => {
      console.log('updated chatlist recieved', data)
    })


    // need to update the message list 
    socket.on("newMessage", data => {
      console.log('new_message', data)
    });
    // setInitialized(true);
  };

  const getMessages = async () => {
    const response = await getChatRoomMessages(getChatData().chatRoomName);
    setMessages(response.data);
    setInitialized(true);
  };

  const getRooms = async () => {
    const response = await getChatRooms();
    setRooms(response.data);
    setInitialized(true);
  };

  useEffect(() => {
    console.log('use_effect')
    if (!initialized) {
      getMessages();
      connectToRoom();
      getRooms();
    }
  });

  return (
    <div className="chat-room-page">
      <h1>
        Chat Room: {getChatData().chatRoomName}. Chat Handle:{" "}
        {getChatData().handle}
      </h1>
      <div className="chat-box">
        {messages.map((m, i) => {
          return (
            <div className="col-12" key={i}>
              <div className="row">
                <div className="col-2">{m.author}</div>
                <div className="col">{m.message}</div>
                <div className="col-3">{m.createdAt}</div>
              </div>
            </div>
          );
        })}
      </div>
      <Formik validationSchema={schema} onSubmit={handleSubmit}>
        {({
          handleSubmit,
          handleChange,
          handleBlur,
          values,
          touched,
          isInvalid,
          errors,
        }) => (
          <Form noValidate onSubmit={handleSubmit}>
            <Form.Row>
              <Form.Group as={Col} md="12" controlId="handle">
                <Form.Label>Message</Form.Label>
                <Form.Control
                  type="text"
                  name="message"
                  placeholder="Message"
                  value={values.message || ""}
                  onChange={handleChange}
                  isInvalid={touched.message && errors.message}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.message}
                </Form.Control.Feedback>
              </Form.Group>
            </Form.Row>
            <Button type="submit" style={{ marginRight: "10px" }}>
              Send
            </Button>
          </Form>
        )}
      </Formik>
    </div>
  );
}

export default ChatRoomPage;

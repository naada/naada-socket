const router = require('express').Router();
const channelModel = require("../../models/Channel");
const Message = require("../../models/Messages");
// const schemas = require('../schemas/auth');
// const userService = require('../../services/user');
// const schemaValidator = require('../../helpers/schemaValidator');
// const passport = require('../../helpers/passport');
// const { generateJWTToken } = require('../../helpers/auth');


router.get('/test', async(req, res)=> {
    return res.json({'msg'  :'chat route working'})
})

router.get("/room/:streamId?", async(req, res) => {
    const streamId = req.params.streamId;
    let chatRoom = await channelModel.find({'streamId':streamId})
    chatRoom = chatRoom[0]
    chatRoom.members = chatRoom.members.filter((member)=>{
        return member.is_active
    })
    return res.json({
        'room': chatRoom
    })
})


router.post("/room", async(req, res) => {
    console.log('hello', req.body)
    const streamId = req.body.streamId;
    try{
        const chatRooms = await channelModel.find({streamId: streamId})
        if(!chatRooms || chatRooms.length == 0){
            const chatRoomObject = {}
            chatRoomObject.streamId = streamId
            const room = await channelModel(chatRoomObject).save()
            return res.json({'room': room})
        }
        return res.json({'room': chatRooms})
    }
    catch(error){
        console.log('err', error)
    }
})

router.post("/room/members", async(req, res) => {
    const streamId = req.body.streamId
    const user = req.body.user
    // console.log(user)
    try{
        const chatRoom = await channelModel.findOneAndUpdate({streamId:streamId}, {$addToSet:{"members":user}}, {new:true})
        console.log(chatRoom, typeof(chatRoom))
        return res.json(chatRoom)
    }
    catch(err){
        console.log(err)
        return res.json(err)
    }
})

router.delete("/room/members", async(req, res) => {
const streamId = req.body.streamId
const userId = req.body.user.userId
// console.log(user)
try{
    const chatRoom = await channelModel.findOneAndUpdate({streamId:streamId, "members.userId": userId}, {$set:{"members.$.exit_at":Date.now(), "members.$.is_active":false}}, {new:true})
    return res.json(chatRoom)
}
catch(err){
    console.log(err)
    return res.json(err)
}

})

router.post("/room/messages/:streamId", async(req, res) => {
    const streamId = req.params.streamId;
    console.log(streamId)
    const message = {}
    message.streamId = streamId
    message.user = req.body.user
    message.message = req.body.message
    const newMessage = await Message(message).save()
    return res.json(newMessage)
})


router.get("/room/messages/:streamId", async(req, res) => {
    const streamId = req.params.streamId;
    const messages = await Message.find({streamId:streamId})
    return res.json(messages)
})


module.exports = router
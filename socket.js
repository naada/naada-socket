const io = require('socket.io')(http);
const connections = OrderedMap()

io.on('connection', (ws) => {
    console.log("Someone connected to server via socket!!")
    const socketID = new ObjectID().toString();
    console.log("New client connected ", socketID);

    const clientConnection  = {
        _id: `${socketID}`,
        ws: ws,
        userId: null,
        isAuthenticated: false
    }

    connections = connections.set(socketID, clientConnection)
})
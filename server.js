const { OrderedMap } = require("immutable");
const userConnections = OrderedMap()
const express = require('express')
const http = require('http');
const socketIO = require('socket.io');
let app = express();
const mongoose = require("mongoose");
let server = http.createServer(app);
let io = socketIO(server, {transports:['websocket']});
const port = process.env.PORT || 3001
const api = require("./api")
const db = require("./config").mogoURI;
const bodyParser = require("body-parser");
const morgan = require("morgan");
const { ENGINE_METHOD_NONE } = require("constants");
const MongoClient = require('mongodb').MongoClient;
const channelModel = require("./models/Channel");
const { update } = require("./models/Channel");
const cors = require('cors');
app.use(cors());

const connectdb = async() => {
    console.log('connect db intialized')
    try{
        await mongoose.connect(db, { useNewUrlParser: true , useUnifiedTopology: true})
    }
    catch(err) {
        throw(err)
    }
}


//  Middlewares

app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


const addUserToRoom = async(streamId, user) => {
    try{
        console.log('add user', streamId, user)
        await channelModel.findOneAndUpdate({streamId:streamId}, {$addToSet:{"members":user}}, {new:true})
    }
    catch(err){
        throw(err)
    }
}

const removeUserFromRomm = async(streamId, user) => {
    try{
        await channelModel.findOneAndUpdate({streamId:streamId, "members.userId": user.userId}, {$set:{"members.$.exit_at":Date.now(), "members.$.is_active":false}}, {new:true})
    }
    catch(err){
        throw(err)
    }
}

const addMessageToRoom= async(chatRoomName, user, message) => {
    const newMessage = {}
    newMessage.streamId = chatRoomName
    newMessage.user = user
    newMessage.message = message
    console.log('new_message', newMessage)
    await Message(newMessage).save()
}

var connections = new OrderedMap()
console.log(Object.keys(io.engine.transports))
io.on('connection', (socket) => {
    connections = connections.set(socket.id)
    socket.on("join", async(data) => {
        // add validation for data.
        // data consists of room_name, user_name, and user_avatar
        const {chatRoomName, user} = data
        console.log('room_joined ', data)
        // when new user joins a room
        // 1. attach the room to the socket connection.
        // 2. add the user to the room in db.
        // 3. Broadcast to everyone in the channel that user has joined.
        // 4. emit the event to update users list in the channel.
        socket.join(chatRoomName)
        user, created = addUserToRoom(chatRoomName, user)
        let payload = {
            action : 'roomJoined',
            message : 'Mr. ${user.username} joined the room'
        }
        io.in(chatRoomName).emit("roomJoined", payload);
        payload = {
            action : 'addUserToChatList',
            user: user
        }
        io.in(chatRoomName).emit("updateChatList", payload);
    })

    socket.on('disconnect', () => {
        // fetch user and chatRoom from socket id.
        // emit event of updateChatList with action remove user from list.
        const {chatRoomName, user} = data
        removeUserFromRomm(chatRoomName, user)
        payload = {
            action : 'removeUserFromChatList',
            user: user
        }
        io.in(chatRoomName).emit("updateChatList", payload)


     })
    socket.on('message', async = (data) => {
        const chatMessage = data.message
        const user = data.user
        const chatRoomName = data.chatRoomName
        // insert message in mongodb.
        addMessageToRoom(chatRoomName, user, chatMessage)
        const payload = {
            action :'messageSent',
            message: chatMessage,
            author: user,
            chatRoomName: chatRoomName,
            createdAt: 'timestamp'
        }
        io.in(chatRoomName).emit("newMessage", payload); 
    })
});



connectdb().then(res=> {
    console.log('db connedted successfully')
    server.listen(port, ()=>{
        console.log(`Server is up on port ${port}`);
      })
}).catch(err => {
    console.log(err)
})
app.use("/api", api);

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const MessageSchema = new Schema({
  message: {
    type: String,
    required: true
  },
  user: {
    userId: {
      type: String, 
      required: true
  },
  userName:{
      type: String,
      required: true
  },
  userAvatar: {
      type: String,  
      default:''
  }
  },
  streamId: { 
    type: String,
    required: true
  },
  created_at: {
    type: Date,
    default: Date.now()
  }
});

module.exports = Message = mongoose.model("messages", MessageSchema);
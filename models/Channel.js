const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ChannelSchema = new Schema({
  members: [{
    _id: false,
    userId: {
        type: String, 
        required: true
    },
    userName:{
        type: String,
        required: true
    },
    userAvatar: {
        type: String,  
        default:''
    },
    entry_at: {
      type: Date,
      default: Date.now()
    },
    exit_at: {
      type: Date,
      default: null
    },
    is_active: {
      type: Boolean,
      default: true
    }
  }
  ],
  streamId: {    // generic id , can be stream id or any id.
    type: String,
    required: true,
    unique: true
  },
});

module.exports = Channel = mongoose.model("channels", ChannelSchema);